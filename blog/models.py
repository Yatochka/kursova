from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default="1" )
    site = models.URLField(blank=True, null=True)
    phone = models.CharField(max_length=13, blank=True, null=True)
    avatar = models.ImageField(blank=True, upload_to='profile_photos/')

    def __str__(self):
        return self.user.username

    class Meta:
        ordering = ('user', 'phone',)

class Chat(models.Model):
    title = models.CharField(max_length=50,blank=False, default="none")
    members = models.ManyToManyField(User,verbose_name="members")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return {'chat_id': self.pk }


class Message(models.Model):
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, default="1")
    author = models.ForeignKey(User, on_delete=models.CASCADE, default="1")
    content = models.TextField(blank=True, null=True, max_length=500)
    timestamp = models.DateTimeField(auto_now=True)
    is_readed = models.BooleanField('Readed', default=False)

    def __str__(self):
        return self.content

    class Meta:
        ordering = ('author', 'timestamp')



class Blog(models.Model):

    author = models.ForeignKey(User, on_delete=models.CASCADE, default="1")
    title = models.CharField(max_length=60, null=False, blank=False, )
    content = models.TextField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('title', 'timestamp')


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, default="1")
    title = models.CharField(max_length=60, null=False, blank=False, )
    content = models.TextField(blank=True, null=True)
    created = models.DateTimeField( auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    likes = models.ManyToManyField(User, blank=True, related_name='post_likes')
    image = models.ImageField(blank=True, upload_to='post_photos/')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        super(Post, self).save(*args, **kwargs)

    class Meta:
        ordering = ('title','created',)


class Publisher(models.Model):
    title = models.CharField(max_length=60, null=False, blank=False)
    description = models.TextField(blank=True, null=True)
    site = models.URLField(blank=True, null=True)
    number = models.CharField(max_length=11, null=True, blank=True)
    email = models.EmailField(blank=True, null=True)
    image = models.ImageField(blank=True, upload_to='publisher_photos/')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        super(Publisher, self).save(*args, **kwargs)

    class Meta:

        ordering = ('title',)


class Comment(models.Model):
    post = models.ForeignKey(Post,on_delete=models.CASCADE, default=1 )
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="1")
    content = models.TextField(max_length=400)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}-{}'.format(self.post.title, str(self.user.username))

    class Meta:
        ordering = ('post',)


class FavoriteBook(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="1")
    book = models.ForeignKey(Post, on_delete=models.CASCADE, default="1")

    class Meta:
        ordering = ('user', 'book', )
