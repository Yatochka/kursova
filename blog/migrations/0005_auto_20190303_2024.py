# Generated by Django 2.1.5 on 2019-03-03 18:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20190303_2023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publisher',
            name='number',
            field=models.CharField(blank=True, max_length=11, null=True),
        ),
    ]
