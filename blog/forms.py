from django import forms
from blog.models import *




class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'body','image')

class PublisherForm(forms.ModelForm):

    class Meta:
        model = Publisher
        fields = ('title', 'description','site','number', 'email', 'image')


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('content')