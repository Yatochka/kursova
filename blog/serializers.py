from django.contrib.auth.models import User, Group
from django.contrib.auth import get_user_model
from rest_framework import serializers
from blog.models import *
from rest_framework.authtoken.models import Token
from rest_auth.serializers import TokenSerializer,UserDetailsSerializer

UserModel = get_user_model()

class UserSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        user = UserModel.objects.create(
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()

        return user
    class Meta:
        model = UserModel
        fields = ('id','url', 'first_name','last_name','username','password', 'email', 'groups',)


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class PostSerializer(serializers.ModelSerializer):

    author = UserSerializer(many=False, read_only=True,)
    title = serializers.CharField(required=True, allow_blank=False, max_length=60)
    content = serializers.CharField(required=True, allow_blank=False, )
    created = serializers.DateTimeField(format="%Y-%m-%d  %H:%M", required=False,read_only=True)
    updated = serializers.DateTimeField(format="%Y-%m-%d  %H:%M", required=False,read_only=True)
    likes = UserSerializer(many=True, read_only=True)
    image = serializers.ImageField()

    def create(self, validated_data):
        return Post.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title',instance.title)
        instance.content = validated_data('content',instance.content)
        instance.updated = validated_data('updated',instance.updated)
        instance.image = validated_data('image', instance.image)

        return instance

    class Meta:
        model = Post
        fields = ('pk','id','url','author','title','content','likes','created','updated','image')


class CommentsSerializer(serializers.ModelSerializer):
    post = PostSerializer(many=False, read_only=True)
    user = UserSerializer(many=False, read_only=True)
    content = serializers.CharField(required=True,allow_blank=False)
    timestamp = serializers.DateTimeField(format="%Y-%m-%d  %H:%M", required=False,read_only=True)
    class Meta:
        model = Comment
        fields = ('url','user','post','content','timestamp')


class PublisherSerializer(serializers.ModelSerializer):
    title = serializers.CharField(required=True, allow_blank=False)
    description = serializers.CharField(required=False, allow_blank=True)
    site = serializers.URLField(required=False,allow_blank=True)
    number = serializers.CharField(required=False,allow_blank=True)
    email = serializers.EmailField(required=False,allow_blank=True)
    image = serializers.ImageField()

    class Meta:
        model = Publisher
        fields = ('id', 'url', 'title', 'description', 'site', 'number', 'email', 'image')

class ChatSerializer(serializers.ModelSerializer):
    title =serializers.CharField(required=True)
    members = UserSerializer(many=True, read_only=True)
    class Meta:
        model = Chat
        fields = ('pk', 'url','title', 'members')




class MessageSerializer(serializers.ModelSerializer):
    chat = ChatSerializer(many=False, read_only=True)
    author = UserSerializer(many=False, read_only=True)
    content = serializers.CharField(required=True,allow_blank=False)
    timestamp = serializers.DateTimeField(format="%Y-%m-%d  %H:%M", required=False,read_only=True)

    class Meta:
        model = Message
        fields = ('pk', 'url', 'chat', 'author', 'content', 'timestamp')

class ProfileSerializer(serializers.ModelSerializer):

    site = serializers.URLField(allow_blank=True)
    phone = serializers.CharField(required=False, allow_blank=True, max_length=13)
    avatar = serializers.ImageField()
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = UserProfile
        fields = ('pk', 'id','site', 'phone', 'user','avatar')

class BlogSerializer(serializers.ModelSerializer):

    author = UserSerializer(many=False, read_only=True, )
    title = serializers.CharField(required=True, allow_blank=False, max_length=60)
    content = serializers.CharField(required=True, allow_blank=False, )
    timestamp = serializers.DateTimeField(format="%Y-%m-%d  %H:%M", required=False, read_only=True)

    class Meta:
        model = Blog
        fields = ('pk','id', 'author', 'title', 'content', 'timestamp')

class FavoriteBookSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False, read_only=True)
    book = PostSerializer(many=False, read_only=True)

    class Meta:
        model = FavoriteBook
        fields = ('pk', 'user', 'book')

class TokenSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False, read_only=True)
    token = TokenSerializer(many=False, read_only=True)
    class Meta:
        model = Token
        fields = ('pk','user','token')