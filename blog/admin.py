from django.contrib import admin
from blog.models import *
# Register your models here.
from rest_framework.authtoken.admin import TokenAdmin

TokenAdmin.raw_id_fields = ('user',)

class PostAdmin(admin.ModelAdmin):

   list_display = ('title', 'author', 'created', 'updated',)
   list_filter = ('title','author','created')
   search_fields = ('author','title',)

class BlogAdmin(admin.ModelAdmin):

   list_display = ('title', 'author', 'timestamp',)
   list_filter = ('title','author','timestamp')
   search_fields = ('author','title',)

class PublisherAdmin(admin.ModelAdmin):
   list_display = ('title','description','site','number', 'email', 'image')
   list_filter = ('title','email')
   search_fields = ('title', 'description', 'email','number', 'site')


class CommentAdmin(admin.ModelAdmin):
   list_display = ('post','user','content','timestamp')
   list_filter = ('post','user','timestamp')
   search_fields = ('post','user','content','timestamp')

class FavoriteAdmin(admin.ModelAdmin):
   list_display = ('user','book',)
   list_filter = ('user',)


admin.site.register(Post, PostAdmin)
admin.site.register(Publisher, PublisherAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(UserProfile)
admin.site.register(Chat)
admin.site.register(Message)
admin.site.register(Blog, BlogAdmin)
admin.site.register(FavoriteBook, FavoriteAdmin)
