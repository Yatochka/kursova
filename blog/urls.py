from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from blog import views
from rest_framework import routers
from rest_framework.authtoken.models import Token
app_name = 'Blog'
'''
urlpatterns = [
    path('posts/', PostListView.as_view()),
    path('posts/create/', PostCreateView.as_view()),
    path('posts/<pk>', PostDetailView.as_view()),
    path('posts/<pk>/update/', PostUpdateView.as_view()),
    path('posts/<pk>/delete/', PostDeleteView.as_view())
]
router = routers.DefaultRouter()


'''


urlpatterns = [

    path('posts', views.PostList.as_view(),name='post-list'),
    path('posts/<int:pk>', views.PostDetail.as_view(),name='post-detail'),
    path('users',views.UserList.as_view(),name='user-list'),
    path('users/<int:pk>',views.UserDetail.as_view(),name='user-detail'),
    path('blogs',views.BlogList.as_view())

]

urlpatterns = format_suffix_patterns(urlpatterns)
