import React from 'react';
import axios from 'axios';

import CustomForm from '../components/BlogForm'

class CreateBlog extends React.Component {




    render() {
        const {form} = this.props
        return(
            <div>
                <h1>Create Blog:</h1>
                <CustomForm requestType="post"
                            blogID = {null}
                            bntText="Create"

                            />
            </div>

        );
    }
};
export default CreateBlog;