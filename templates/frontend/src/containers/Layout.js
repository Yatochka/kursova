import React, { Component } from 'react';
import { Layout, Menu, Input,Badge, Dropdown } from 'antd';
import {Link, withRouter} from "react-router-dom";
import { connect } from 'react-redux';
import * as actions from '../store/actions/auth';
import axios from "axios";

const { Header, Content, Footer } = Layout;
const Search = Input.Search;

class CustomLayout extends React.Component {

     state = {
         count: 5,
         user: {}
     }
    componentDidMount() {
         const ses_token = localStorage.getItem('token');
         axios.get(`http://127.0.0.1:8000/tokens/${ses_token}/`)
             .then( res =>{
                 this.setState({
                      user: res.data.user
                 });

             });

    }

    render() {
         const menu = (
                  <Menu>
                      <Menu.Item>
                      <Link to="/create-blog">New Blog</Link>
                    </Menu.Item>
                    <Menu.Item>
                      <Link to="/settings">Settings</Link>
                    </Menu.Item>
                   <Menu.Item>
                       <Badge count={this.state.count}><Link to="/chat">Messages</Link>
                    </Badge></Menu.Item>
                    <Menu.Item onClick={this.props.logout}>
                                    Exit
                    </Menu.Item>
                  </Menu>
    );
    console.log("user: " + this.state.user);
    console.log("local user:" +  localStorage.getItem('user'));
         return(

        <Layout className="layout">
            <Header>
              <div className="logo" />
                  <Menu
                    theme="dark"
                    mode="horizontal"
                    defaultSelectedKeys={['1']}
                    style={{ lineHeight: '64px' }}
                  >
                      <Menu.Item key="1"><Link to="/">Home</Link></Menu.Item>
                      <Menu.Item key="2"><Link to="/blogs">Blogs</Link></Menu.Item>
                      <Menu.Item key="3"><Link to="/create">Create</Link></Menu.Item>
                      {
                        this.props.isAuthenticated ?
                        <Menu.Item key="7" style = {{float:'right'}}> <Dropdown overlay={menu}><Link to={`/user/${this.state.user.id}`}>Profile</Link></Dropdown></Menu.Item>
                        :
                        <Menu.Item key="7" style = {{float:'right'}}>
                          <Link to="/login">Login</Link>
                        </Menu.Item>
                      }
                     
                      <Menu.Item key="8"  style = {{float:'right'}}> <Search
                             placeholder="Search..."
                             style = {{paddingTop: '20px' }}
                             onSearch={value => console.log(value)}
                             enterButton
                      /></Menu.Item>


                  </Menu>
            </Header>
            <Content style={{ padding: '0 50px' }}>
              <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                  {this.props.children}
              </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>
              LNU ©2019 Created by Michael Benko
            </Footer>
          </Layout>
    );
            }


}
const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout())
    }
}

export default withRouter(connect(null, mapDispatchToProps)(CustomLayout));
