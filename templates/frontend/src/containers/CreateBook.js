import React from 'react';
import axios from 'axios';

import CustomForm from '../components/Form'

class CreateBook extends React.Component {




    render() {
        const {form} = this.props
        return(
            <div>
                <h1>Create Book:</h1>
                <CustomForm requestType="post"
                            bookID = {null}
                            bntText="Create"
                            />
            </div>

        );
    }
};
export default CreateBook;