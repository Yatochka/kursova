import React from 'react';
import axios from 'axios';
import { Comment, Tooltip, Form, Input, Button, Card} from "antd";
import {List} from "antd/lib/list";
import './detail.css'
import moment from 'moment';

const TextArea = Input.TextArea;
const FormItem = Form.Item;


const CommentList = ({ comments }) => (
  <List
    dataSource={comments}
    header={`${comments.length} ${comments.length > 1 ? 'replies' : 'reply'}`}
    itemLayout="horizontal"
    renderItem={props => <Comment {...props} />}
  />
);



class BooksDetail extends React.Component {

     state = {
        book : {},
        comments: [],

    };


    componentDidMount() {
         const bookID = this.props.match.params.bookID;
         axios.get(`http://127.0.0.1:8000/posts/${bookID}/`)
             .then( res =>{
                 this.setState({
                     book  : res.data
                 });
             });
    }
    handleDelete = event => {
    const bookID = this.props.match.params.bookID;
    axios.delete(`http://127.0.0.1:8000/posts/${bookID}`)
    this.props.history.push(`/`);
    this.forceUpdate()

  }


    render() {
        console.log(this.state.book);
        const { comments, submitting, value } = this.state;

        return(
        <div>
            <Card hoverable  >

                <img style={{ width:250, }} id="img" src ={this.state.book.image}/>
                <h1>{this.state.book.title}</h1>
                <hr/>
                <p style={{textAlign: 'center'}} >{this.state.book.content}</p>
                <hr/>
                <form onSubmit={this.handleDelete}>
                    <Button type="danger" htmlType="submit" style={{float :'right'}}>
                        Delete
                    </Button>
                </form>

            </Card>
            <Card>
             <Form  onSubmit={(event) => this.handleFormSubmit(
                    event,
                    )}>
                    <FormItem>
                        <h4>Comment:</h4>
                        <TextArea name="conent" autosize={{minRows:5, maxRows:10}}/>
                    </FormItem>

                    <FormItem>
                        <Button type="primary" htmlType="submit" style={{float:"right"}}>
                            Comment
                        </Button>
                    </FormItem>
                </Form>


            </Card>
    </div>
        );
    }
};
export default BooksDetail;