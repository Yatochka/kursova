import React from 'react';
import axios from 'axios';

import Book from '../components/Book'

class BooksList extends React.Component {

     state = {
        books: []
    }


     componentDidMount() {
         axios.get("http://127.0.0.1:8000/api/posts")
             .then( res =>{
                 this.setState({
                     books: res.data
                 })
             })
    }

    render() {
         console.log(this.state.books);
        return(
            <Book data= {this.state.books}/>
        );
    }
};
export default BooksList;