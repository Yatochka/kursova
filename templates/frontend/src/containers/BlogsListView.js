import React from 'react'
import {Component} from 'react'
import {Link} from "react-router-dom";
import axios from "axios";
import Blog from "../components/Blog"

class BlogsListView extends Component{


     state = {
        blogs: [],
         author: {}
    }


     componentDidMount() {
         axios.get("http://127.0.0.1:8000/api/blogs")
             .then( res =>{
                 this.setState({
                     blogs: res.data,
                     author: res.data.author
                 })
             })
    }

    render() {
         console.log(this.state.blogs);
         console.log(this.state.author);
        return(
            <Blog data= {this.state.blogs}/>
        );
    }
}

export default BlogsListView;