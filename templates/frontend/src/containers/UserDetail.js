import React from 'react';
import axios from 'axios';
import {Button, Card} from "antd";
import {List} from "antd/lib/list";
import './detail.css'
import {Link} from "react-router-dom";

class UserDetail extends React.Component {

     state = {
       profile : {},
       user: {},
    };

    componentDidMount() {

         const userID = this.props.match.params.userID;
         axios.get(`http://127.0.0.1:8000/profiles/${userID}/`)
             .then( res =>{
                 this.setState({
                     profile: res.data,
                     user: res.data.user

                 });
             });
    }




    render() {
        console.log(this.state.profile.user);
        return(

            <Card hoverable  ><div> <img style={{ width:300, }} id="img" src={this.state.profile.avatar}/>
               </div>
               <h1>{this.state.user.username}</h1>
                <hr/>
                <div style={{textAlign:'center'}} >
                    <h4>Name: {this.state.user.first_name}</h4>
                    <h4>Surname: {this.state.user.last_name}</h4>
                    <p>Email: {this.state.user.email}</p>
                    <p>Phone: {this.state.profile.phone}</p>
                    <p>Site: {this.state.profile.site}</p>

                </div>
                <hr/>
                <Button style={{ float:'right'}} type="primary"><Link to="/">Message</Link></Button>

            </Card>
        );
    }
};
export default UserDetail;