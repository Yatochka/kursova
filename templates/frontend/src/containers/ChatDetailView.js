import React from "react"
import {Component} from "react"
import Messages from "../components/Messages"
import MessageInput from "../components/MessageInput"
import { Input,  List} from "antd";
import './detail.css'

const TextArea = Input.TextArea;

const data = [
  {
    title: 'Bob',
    avatar: 'https://cdn.iconscout.com/icon/free/png-256/avatar-375-456327.png',
    mess:'Hi! It is a test!',
  },
  {
    title: 'Dilan',
    avatar: 'https://cdn.iconscout.com/icon/free/png-256/avatar-367-456319.png',
    mess:'Hi! Ok!',
  },
  {
    title: 'Bob',
    avatar: 'https://cdn.iconscout.com/icon/free/png-256/avatar-375-456327.png',
    mess:'How your wife,Dilan?',
  },
  {
    title: 'Dilan',
    avatar: 'https://cdn.iconscout.com/icon/free/png-256/avatar-367-456319.png',
    mess:'She is okay, thanks',
  },
];


class ChatDetailView extends Component {
render() {
    return (
        <div>
            <List
                itemLayout="horizontal"
                dataSource={data}
                renderItem={item => (
                    <List.Item>
                        <Messages
                            avatar={item.avatar}
                            title={item.title}
                            mess={item.mess}
                        />
                    </List.Item>)}
            />
            <MessageInput/>
        </div>

    );
}

}
export default ChatDetailView;