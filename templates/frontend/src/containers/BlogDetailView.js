import React from 'react'
import {Component} from 'react'
import axios from 'axios';
import { Form, Input, Button, Card} from "antd";
import {List} from "antd/lib/list";
import './detail.css'

const TextArea = Input.TextArea;
const FormItem = Form.Item;

class BlogDetailView extends Component{
     state = {
        blog : {},
        comments: [],
         author: {}

    };
    handleFormSubmit(){

    }

    componentDidMount() {
         const blogID = this.props.match.params.blogID;
         axios.get(`http://127.0.0.1:8000/blogs/${blogID}/`)
             .then( res =>{
                 this.setState({
                     blog  : res.data,
                     author : res.data.author
                 });
             });
    }
    handleDelete = event => {
    const blogID = this.props.match.params.blogID;
    axios.delete(`http://127.0.0.1:8000/blogs/${blogID}`)
    this.props.history.push(`/`);
    this.forceUpdate()

  }

    render() {
        return(
            <div>
            <Card hoverable  >

                <h1>{this.state.blog.title} <span style={{float:"right"}}>{this.state.author.user.username}</span></h1>
                <hr/>
                <p style={{textAlign: 'center'}} >{this.state.blog.content}</p>
                <hr/>
                <form onSubmit={this.handleDelete}>
                    <Button type="danger" htmlType="submit" style={{float :'right'}}>
                        Delete
                    </Button>
                </form>

            </Card>
            <Card>
             <Form  onSubmit={(event) => this.handleFormSubmit(
                    event,
                    )}>
                    <FormItem>
                        <h4>Comment:</h4>
                        <TextArea name="conent" autosize={{minRows:5, maxRows:10}}/>
                    </FormItem>

                    <FormItem>
                        <Button type="primary" htmlType="submit" style={{float:"right"}}>
                            Comment
                        </Button>
                    </FormItem>
                </Form>


            </Card>
    </div>
        )
    }
}

export default BlogDetailView;