import React from 'react';
import {Route} from 'react-router-dom';
import BookList from './containers/BooksListView'
import BookDetail from './containers/BookDetailView'
import CreateBook from './containers/CreateBook'
import CreateBlog from './containers/CreateBlog'
import UserDetail from './containers/UserDetail'
import ChatDetailView from './containers/ChatDetailView'
import Settings from './containers/Settings'
import BlogsListView from './containers/BlogsListView'
import BlogDetailView from './containers/BlogDetailView'
import ChatList from './containers/ChatsList'
import NormalLoginForm from './containers/Login'
import RegistrationForm from './containers/Signup'

const BaseRouter = () =>(
    <div>
        <Route exact path='/' component={BookList} />
        <Route exact path='/book/:bookID/' component={BookDetail} />
        <Route exact path='/create/' component={CreateBook}/>
        <Route exact path='/user/:userID' component={UserDetail}/>
        <Route exact path='/chat/:chatID' component={ChatDetailView}/>
        <Route exact path='/chat' component={ChatList}/>
        <Route exact path='/settings/' component={Settings}/>
        <Route exact path='/blogs/' component={BlogsListView}/>
        <Route exact path='/blog/:blogID/' component={BlogDetailView} />
        <Route exact path='/create-blog/' component={CreateBlog}/>
        <Route exact path='/login/' component={NormalLoginForm}/>
        <Route exact path='/signup/' component={RegistrationForm}/>
    </div>
);
export default BaseRouter;