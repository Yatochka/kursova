import {Component} from "react";
import React from "react";
import { List, Avatar } from 'antd';

class Messages extends Component {
render() {
    return(
        <List.Item.Meta
                  avatar={<Avatar src={this.props.avatar} />}
                  title={<a href="https://ant.design">{this.props.title}</a>}
                  description={this.props.mess}
        />
    )
}
}

export default Messages;