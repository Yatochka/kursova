import React from 'react';
import { List, Avatar, Icon, Button,  } from 'antd';
import {Link} from "react-router-dom";




const IconText = ({ type, text }) => (
  <span>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text}
  </span>
);


const Book = (props) => {
   return (
            <List
            itemLayout="vertical"
            size="large"
            pagination={{
              onChange: (page) => {
                console.log(page);
              },
              pageSize: 10,
            }}
            dataSource={props.data}
            renderItem={item => (

              <List.Item
                key={item.title}
                actions={[<IconText type="like-o" text="1" />, <IconText type="message" text="2" />]}
                extra={<p>Date: {item.timestamp}</p>}
              >
                <List.Item.Meta
                  description = {<Link to={`/user/${item.author.id}`}>by: {item.author.username}</Link>}
                  title={<a href={`blog/${item.id}`}>{item.title}</a>}/>
                <div >
                    {item.content}
                </div>

                <p>id: {item.id}</p>

              </List.Item>
            )}
          />
   );
};

export default Book;