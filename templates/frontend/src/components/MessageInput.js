import {Component} from "react";
import React from "react";
import {Button, Form, Input} from "antd";

const { TextArea } = Input;
const FormItem = Form.Item;


class MessageInput extends Component {
   state = {
    text: ""
  }


  render() {
    return (
            <Form >

                    <FormItem>
                        <TextArea name="body" autosize={{minRows:3,}}/>
                    </FormItem>
                    <FormItem>
                        <Button type="primary" htmlType="submit" style = {{float:"right"}}>
                           Send
                        </Button>
                    </FormItem>
                </Form>
    );
  }
}

export default MessageInput;