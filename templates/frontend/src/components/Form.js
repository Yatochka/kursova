import React from 'react'
import {Form, Input, Button, Icon, Upload } from "antd";
import axios from 'axios'

const { TextArea } = Input;
const FormItem = Form.Item;

class CustomForm extends React.Component{


       handleFormSubmit = (event, requestType, bookID) =>{
           event.preventDefault();
           const postObj = {
               title:event.target.elements.title.value,
               content: event.target.elements.content.value
           }

            if(requestType == 'post'){
                axios.post("http://127.0.0.1:8000/api/posts",  postObj )
                                   .then(res=> console.log(res))
                                   .catch(err=>console.error(err))

            }
            else if (requestType == 'put')
            {
                 axios.put(`http://127.0.0.1:8000/posts/${bookID}`,  postObj)
                                   .then(res=> console.log(res))
                                   .catch(err=>console.error(err))

            }
       }


    render() {
        console.log(this.props);

        return(

            <div>
                <Form  onSubmit={(event) => this.handleFormSubmit(
                    event,
                    this.props.requestType,
                    this.props.bookID
                    )}>
                    <FormItem>
                        <h2>Title:</h2>
                        <Input name ="title"/>
                    </FormItem>
                    <FormItem>
                        <h2>Story:</h2>
                        <TextArea name="content" autosize={{minRows:20,}}/>
                    </FormItem>

                    <FormItem>
                        <Button type="primary" htmlType="submit" style = {{float:"right"}}>
                            {this.props.bntText}
                        </Button>
                    </FormItem>
                </Form>
            </div>
        )
    }

}

export default CustomForm;